
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

function trenutniEHR(){
  return document.getElementById('textAreaId').value;
}

function generirajVsePodatke(){
  generirajPodatke(1, function(ehrId1){
    generirajPodatke(2, function(ehrId2){
      generirajPodatke(3, function(ehrId3){
        var o1 = '<option class="dropdown-content option" value="'+ehrId1+'">Bojana Banana</option>';
        var o2 = '<option class="dropdown-content option" value="'+ehrId2+'">Kevin Kokos</option>';
        var o3 = '<option class="dropdown-content option" value="'+ehrId3+'">Lara Luža</option>';
        document.getElementById("dropDownId").innerHTML = o1 + o2 + o3;
        document.getElementById("textAreaId").value = ehrId1;
        
      });  
    });    
  });
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  switch(stPacienta){
    case 1:
      pridobiEHRInVstaviBolnika("Bojana", "Bojana", "1999-01-01", function(ehrId){
        dodajPodatke(ehrId, "2005-01-01T12:34:56", 160, 85, function(){
          dodajPodatke(ehrId, "2006-01-01T12:34:56", 180, 80, function(){
            dodajPodatke(ehrId, "2007-01-01T12:34:56", 180, 77, function(){
              dodajPodatke(ehrId, "2008-01-01T12:34:56", 180, 80, function(){
                dodajPodatke(ehrId, "2009-01-01T12:34:56", 180, 80, function(){
                  callback(ehrId); 
                });
              });
            });  
          });  
        });  
      });
      break;
    case 2:
      pridobiEHRInVstaviBolnika("Kevin", "Kokos", "1999-01-01", function(ehrId){
        dodajPodatke(ehrId, "2003-01-01T12:34:56", 175, 90, function(){
          dodajPodatke(ehrId, "2004-01-01T12:34:56", 180, 80, function(){
            dodajPodatke(ehrId, "2005-01-01T12:34:56", 180, 77, function(){
              dodajPodatke(ehrId, "2006-01-01T12:34:56", 180, 80, function(){
                dodajPodatke(ehrId, "2007-01-01T12:34:56", 180, 80, function(){
                  callback(ehrId);   
                });
              });
            });  
          });  
        });  
      });
      break;
    case 3:
      pridobiEHRInVstaviBolnika("Lara", "Luža", "1999-01-01", function(ehrId){
        dodajPodatke(ehrId, "2005-01-01T12:34:56", 180, 70, function(){
          dodajPodatke(ehrId, "2006-01-01T12:34:56", 180, 84, function(){
            dodajPodatke(ehrId, "2007-01-01T12:34:56", 180, 80, function(){
              dodajPodatke(ehrId, "2008-01-01T12:34:56", 182, 85, function(){
                dodajPodatke(ehrId, "2009-01-01T12:34:56", 180, 80, function(){
                  callback(ehrId);   
                });
              });
            });  
          });  
        });  
      });
      break;
  }
}

function pridobiEHRInVstaviBolnika(ime, priimek, datumRojstva, callback){
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {"Authorization": getAuthorization()},
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {"Authorization": getAuthorization()},
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              callback(ehrId);
            }
          }
        });
      }
		});
}

function preberiOsnovnePodatke(ehrId, callback){
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			callback(party); // json objekt, isto format kot partyData v pridobiEHR... metodi
  		}
		});  
}

function dodajPodatke(ehrId, datumInUra, telesnaVisina, telesnaTeza, callback){
  var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        callback();  
      }
		});
}

function teze(callback){
  $.ajax({
    				  url: baseUrl + "/view/" + trenutniEHR() + "/weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      callback(res);	
    			    }
  					});
}

function visine(callback){
  $.ajax({
    				  url: baseUrl + "/view/" + trenutniEHR() + "/height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      callback(res);	
    			    }
  					});
}

function izracunaj(){
  teze(function(teze){
    visine(function(visine){
      //teze in visine sta JSONArray-a
      for(var i = 0; i < teze.length; i++){
        var datumMeritve = teze[i].time;
        var teza = teze[i].weight;
      }
      
      for(var i = 0; i < visine.length; i++){
        var datumMeritve = visine[i].time;
        var visina = visine[i].height;
      }
      document.getElementById("sporocilo").innerText = "Zandja izmerjena visina: "+visine[0].height+" Zadnja izmerjena teza: "+teze[0].weight;
      document.getElementById("X").innerText = visine[0].height-100;
      document.getElementById("razlika").innerText = Math.abs((visine[0].height-100)-teze[0].weight);
    })
  }) 
}

//BOLNISNICE

function narisiZemljevid(){
  
  var blue = {
    color: "blue"
  }

  var green = {
    color: "green"
  }

  // Ustvarimo objekt mapa
  mapa = new L.map('zemljevid', {
    center: [46.0536077,14.5247825],
    zoom: 12
  });
  
  var url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

  var poligoni = []

  mapa.addLayer(new L.TileLayer(url));

  function koKlikneš(e) {
    var ll = e.latlng;
    for(var pol in poligoni){
      poligoni[pol].setStyle(distance(ll.lat, ll.lng, poligoni[pol]._latlngs[0][0].lat, poligoni[pol]._latlngs[0][0].lng, "K") < 0.4 ? green : blue);
    }
  } 

  mapa.on('click', koKlikneš);
  
  pridobiPodatkeZaBolnice(function(bolnice){
    var feat = bolnice.features;
    for(var i = 0; i < feat.length; i++){
      var bol = feat[i];

      if(bol.geometry.type !== "Polygon") {
        continue;
      }
      var props = bol.properties;
      var c = bol.geometry.coordinates;
      
      for(var j = 0; j < c.length; j++){
        var crds = c[j];
        for(var k = 0; k < crds.length; k++){
          var arr = crds[k]
          var tmp = arr[0];
          arr[0] = arr[1];
          arr[1] = tmp;
          crds[k]=arr;
        }  
        c[j] = crds;
      }

      var p = L.polygon(c, blue);
    
      p.addTo(mapa);
    
      p.bindPopup("IME: "+props.name+"<br>NASLOV: "+props["addr:street"]+" "+props["addr:housenumber"]);
    
      poligoni.push(p);
    } 
    
    
  })
  
}

function pridobiPodatkeZaBolnice(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        callback(json);
    }
  };
  xobj.send(null);
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
